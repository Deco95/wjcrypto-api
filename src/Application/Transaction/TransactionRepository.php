<?php
declare(strict_types=1);

namespace Src\Application\Transaction;

use Src\Infrastructure\Repository\EloquentRepository;

/**
 * Class TransactionRepository
 * @package Src\Application\Transaction
 */
class TransactionRepository extends EloquentRepository implements TransactionRepositoryInterface
{
    /** @var TransactionModel */
    protected $modelClass;

    /**
     * TransactionRepository constructor.
     * @param TransactionModel $model
     */
    public function __construct(TransactionModel $model)
    {
        $this->modelClass = $model;
    }

    /**
     * TransactionRepository constructor.
     * @param int $userAccount
     * @return int
     */
    public function getAccountCredit(int $userAccount): int
    {
        $params = [
            'account_credit' => $userAccount,
        ];
        $query = $this->newQuery();
        $items = $query->where($params)->get();
        return $items->sum('credit');
    }

    /**
     * TransactionRepository constructor.
     * @param int $userAccount
     * @return int
     */
    public function getAccountDebit(int $userAccount): int
    {
        $params = [
            'account_debit' => $userAccount,
        ];
        $query = $this->newQuery();
        $items = $query->where($params)->get();
        return $items->sum('debit');
    }
}
