<?php
declare(strict_types=1);

namespace Src\Application\Transaction;

use Src\Infrastructure\Models\BaseModel;

/**
 * Class to use Eloquent ORM.
 *
 * Class TransactionModel
 * @package Src\Application\Transaction
 */
class TransactionModel extends BaseModel
{
    /**
     * The database's table name.
     *
     * @var string
     */
    protected $table = 'transaction';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_type',
        'transaction_status',
        'account_debit',
        'account_credit',
        'debit',
        'credit',
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';
}
