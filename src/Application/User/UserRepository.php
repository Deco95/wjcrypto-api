<?php
declare(strict_types=1);

namespace Src\Application\User;

use Illuminate\Database\Eloquent\Model;
use Src\Infrastructure\Repository\EloquentRepository;
use Src\Units\Exceptions\BaseException;

/**
 * Class UserRepository
 * @package Src\Application\User
 */
class UserRepository extends EloquentRepository implements UserRepositoryInterface
{
    /** @var UserModel */
    protected $modelClass;

    /**
     * UserRepository constructor.
     * @param UserModel $model
     */
    public function __construct(UserModel $model)
    {
        $this->modelClass = $model;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data = []): Model
    {
        $model = $this->factory($data);
        return $this->save($model);
    }

    /**
     * @param int $email
     * @param int $pass
     * @return array
     */
    public function auth($email, $pass): ?array
    {
        if (!$email or !$pass)
            return null;
        $params = [
            'email' => $email,
            'password' => $pass,
        ];
        $query = $this->newQuery();
        $item = $query->where($params)->first();
        if (!$item)
            return null;
        return [
            'userId' => $item->getAttribute('id'),
            'account_number' => $item->getAccountNumberAttribute(),
        ];
    }

    /**
     * @param $taxvat
     * @return int
     * @throws BaseException
     */
    public function getUserAccountByTaxVat($taxvat): int
    {
        $params = [
            'taxvat' => $taxvat,
        ];
        $query = $this->newQuery();
        $item = $query->where($params)->first();
        if (!$item)
            throw new BaseException('Failed to find Currency Account by TaxVat');
        return $item->getAccountNumberAttribute();
    }
}
