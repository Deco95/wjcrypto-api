<?php
declare(strict_types=1);

namespace Src\Application\User;

use Src\Application\CurrencyBalance\CurrencyBalanceService;
use Src\Infrastructure\Models\BaseModel;
use Src\Infrastructure\Services\BaseService;
use Src\Infrastructure\Validator\ValidatorInterface;
use Src\Units\Exceptions\BaseException;
use Src\Units\Exceptions\ValidationException;

/**
 * Class UserService
 * @package Src\Application\User
 */
class UserService extends BaseService
{
    /** @var string */
    protected $currencyBalanceService = CurrencyBalanceService::class;

    /**
     * UserService constructor.
     * @param BaseModel $modelClass
     * @param ValidatorInterface $validatorClass
     * @param UserRepositoryInterface $repositoryClass
     */
    public function __construct(
        BaseModel $modelClass = null,
        ValidatorInterface $validatorClass = null,
        UserRepositoryInterface $repositoryClass = null
    )
    {
        $this->modelClass = $modelClass;
        $this->validatorClass = $validatorClass;
        $this->repositoryClass = $repositoryClass;
        if ($modelClass === null)
            $this->modelClass = UserModel::class;
        if ($validatorClass === null)
            $this->validatorClass = UserValidator::class;
        if ($repositoryClass === null)
            $this->repositoryClass = UserRepository::class;
    }

    /**
     * @param int $email
     * @param int $pass
     * @return array
     */
    public function auth($email, $pass): ?array
    {
        return $this->newRepository()->auth($email, $pass);
    }

    /**
     * @param $taxvat
     * @return int
     */
    public function getUserAccountByTaxVat($taxvat): int
    {
        return $this->newRepository()->getUserAccountByTaxVat($taxvat);
    }

    /** Create new User
     *
     * @param array $body
     * @return array
     * @throws ValidationException
     * @throws \Src\Units\Exceptions\BaseException
     */
    public function create(array $body): array
    {
        $this->validate($this->validatorClass::CREATE_MODE, $body);
        $user = $this->newRepository()->create($body);
        $userAccount = $user->getAccountNumberAttribute();

        $this->newCurrencyBalanceService()->newCurrencyBalance($userAccount);
        return $user->toArray();
    }

    /**
     * @return CurrencyBalanceService
     */
    protected function newCurrencyBalanceService(): CurrencyBalanceService
    {
        return new $this->currencyBalanceService;
    }

    /** Update user with
     *
     * @param array $body
     * @return array
     * @throws ValidationException
     * @throws \Exception
     */
    public function update(array $body): array
    {
        $sessionUserId = $this->getUserId();
        $this->validate($this->validatorClass::UPDATE_MODE, $body, $sessionUserId);
        $user = $this->newRepository()->update($sessionUserId, $body);
        return $user->toArray();
    }

    /** Update user with
     *
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function getUser(int $id = null): array
    {
        if (!$id)
            $id = $this->getUserId();
        $user = $this->newRepository()->getById($id);
        return $user->toArray();
    }

    /** Update user with
     *
     * @param int $id $id
     * @return bool
     */
    public function exists(int $id): bool
    {
        try {
            $this->newRepository()->getById($id)->toArray();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
}
