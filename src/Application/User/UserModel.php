<?php
declare(strict_types=1);

namespace Src\Application\User;

use Src\Infrastructure\Models\BaseModel;

/**
 * Class to use Eloquent ORM.
 *
 * Class UserModel
 * @package Src\Application\User
 */
class UserModel extends BaseModel
{
    /**
     * The database's User's Account field.
     *
     * @var string
     */
    const TABLE_ID = 'id';

    const ACCOUNT_NUMBER = 'account_number';
    const NAME = 'name';
    const EMAIL = 'email';
    const TAX_VAT = 'taxvat';
    const PASSWORD = 'password';
    const REGISTRY = 'registry';
    const BIRTHDAY = 'birthday';
    const PHONE = 'phone';
    const ADDRESS = 'address';

    /**
     * The database's table name.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ACCOUNT_NUMBER,
        self::NAME,
        self::EMAIL,
        self::TAX_VAT,
        self::PASSWORD,
        self::REGISTRY,
        self::BIRTHDAY,
        self::PHONE,
        self::ADDRESS,
    ];

    /**
     * @var string
     */
    protected $primaryKey = self::TABLE_ID;

    public function toArray()
    {
        $array = parent::toArray();
        unset($array[self::PASSWORD]);

        return $array;
    }

    /**
     * @return int
     */
    public function getAccountNumberAttribute() {
        // TODO create view in mysql for user and use it with Eloquent to catch the account_number
        return $this->getAttribute(self::TABLE_ID) + 10000;
    }
}
