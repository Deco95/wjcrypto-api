<?php
declare(strict_types=1);

namespace Src\Application\User;

use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Src\Infrastructure\Controllers\BaseController;

/**
 * Class CurrencyController
 * @package Src\Application\Currency
 */
class UserController extends BaseController
{

    /** @var string */
    protected $serviceClass = UserService::class;
    /** @var string */
    protected $modelClass = UserModel::class;

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getUser(ServerRequestInterface $request) : ResponseInterface
    {
        $user = $this->newService()->getUser();
        return $this->buildResponse($user, 200);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function register(ServerRequestInterface $request) : ResponseInterface
    {
        $body = $this->getParsedBody($request);

        $user = $this->newService()->create($body);
        return $this->buildResponse($user, 201);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function update(ServerRequestInterface $request) : ResponseInterface
    {
        $body = $this->getParsedBody($request);

        $user = $this->newService()->update($body);
        return $this->buildResponse($user, 200);
    }

    /**
     * @param ServerRequestInterface $request
     * @return array
     * @throws \Exception
     */
    public function getParsedBody(ServerRequestInterface $request) : array
    {
        $body = $request->getParsedBody();
        return $this->hashPassword($body);
    }

    /**
     * @param array $body
     * @return array
     * @throws \Exception
     */
    public function hashPassword(array $body) : array
    {
        $passString = $this->modelClass::PASSWORD;
        if (in_array($passString, $body))
            $body[$passString] = md5($body[$passString]);
        return $body;
    }
}
