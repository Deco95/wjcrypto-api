<?php
declare(strict_types=1);

namespace Src\Application\User;

use Src\Infrastructure\Validator\BaseValidator;
use Src\Units\FieldValidator;

/**
 * Class CategoryValidator
 * @package Src\Validator
 */
class UserValidator extends BaseValidator
{
    /**
     * CategoryValidator constructor.
     * @param string|null $mode
     * @param int|null    $modelId
     */
    public function __construct(?string $mode, ?int $modelId = null)
    {
        parent::__construct($mode, $modelId);
        $this->model = new UserModel();
    }

    /**
     * @return array
     */
    public function getCreateValidator(): array
    {
        return [
            'name' => new FieldValidator(false, true,  'string,integer', false),
            'email' => new FieldValidator(false, true,  'string,integer', true),
            'password' => new FieldValidator(false, true,  'string,integer', false),
            'taxvat' => new FieldValidator(false, true,  'string,integer', true),
            'registry' => new FieldValidator(false, true,  'string,integer', true),
            'birthday' => new FieldValidator(false, true,  'string,integer', false),
            'phone' => new FieldValidator(false, true,  'string,integer', false),
            'address' => new FieldValidator(false, true,  'string,integer', false),
        ];
    }

    /**
     * @return array
     */
    public function getUpdateValidator(): array
    {
        return [
            'name' => new FieldValidator(false, false,  'string,integer', false),
            'email' => new FieldValidator(false, false,  'string,integer', true),
            'password' => new FieldValidator(false, false,  'string,integer', false),
            'taxvat' => new FieldValidator(false, false,  'string,integer', true),
            'registry' => new FieldValidator(false, false,  'string,integer', true),
            'birthday' => new FieldValidator(false, false,  'string,integer', false),
            'phone' => new FieldValidator(false, false,  'string,integer', false),
            'address' => new FieldValidator(false, false,  'string,integer', false),
        ];
    }
}
