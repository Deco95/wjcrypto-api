<?php
declare(strict_types=1);

namespace Src\Application\CurrencyBalance;

use Src\Infrastructure\Repository\EloquentRepository;
use Src\Units\Exceptions\BaseException;

/**
 * Class CurrencyRepository
 * @package Src\Application\Currency
 */
class CurrencyBalanceRepository extends EloquentRepository implements CurrencyBalanceRepositoryInterface
{
    /** @var CurrencyBalanceModel */
    protected $modelClass;

    /**
     * The database's User's Account field.
     *
     * @var string
     */
    const ACCOUNT_NUMBER_NOT_FOUND_MESSAGE = 'User Currency Balance Not Found';

    /**
     * CurrencyRepository constructor.
     * @param CurrencyBalanceModel $model
     */
    public function __construct(CurrencyBalanceModel $model)
    {
        $this->modelClass = $model;
    }

    /**
     * @param int $userAccount
     * @return int
     * @throws BaseException
     */
    public function getBalance(int $userAccount): int
    {
        $this->accountExistsOrFail($userAccount);
        $params = [
            $this->modelClass::ACCOUNT_NUMBER => $userAccount,
        ];
        $query = $this->newQuery();
        $item = $query->where($params)->first();
        return $item->getAttribute('total_balance');
    }

    /**
     * @param int $userAccount
     * @param int $balance
     * @return int
     * @throws BaseException
     */
    public function setBalance(int $userAccount, int $balance): int
    {
        $this->accountExistsOrFail($userAccount);
        $params = [
            $this->modelClass::ACCOUNT_NUMBER => $userAccount,
        ];
        $query = $this->newQuery();
        $item = $query->where($params)->first();
        $item->setAttribute('total_balance',$balance);
        $item->save();
        return $item->getAttribute('total_balance');
    }

    /**
     * @param int $value
     * @param int $userAccount
     * @return bool
     * @throws BaseException
     */
    public function hasCurrency($value, int $userAccount): bool
    {
        $this->accountExistsOrFail($userAccount);
        $UserBalance = $this->getBalance($userAccount);

        if (!$UserBalance)
            return false;
        if ($UserBalance>=$value)
            return true;
        return false;
    }

    /**
     * @param int $userAccount
     * @return bool
     */
    public function accountExists(int $userAccount): bool
    {
        $params['account_number'] = $userAccount;
        $query = $this->newQuery();
        try {
            $query->where($params)->firstOrFail();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param int $userAccount
     * @return void
     * @throws BaseException
     */
    public function accountExistsOrFail(int $userAccount): void
    {
        $exists = $this->accountExists($userAccount);
        if (!$exists)
            throw new BaseException(self::ACCOUNT_NUMBER_NOT_FOUND_MESSAGE);
    }
}
