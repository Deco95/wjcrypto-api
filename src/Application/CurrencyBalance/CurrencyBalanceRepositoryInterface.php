<?php
declare(strict_types=1);

namespace Src\Application\CurrencyBalance;

use Src\Infrastructure\Repository\RepositoryInterface;

/**
 * Interface CurrencyRepositoryInterface
 * @package Src\Application\Currency
 */
interface CurrencyBalanceRepositoryInterface extends RepositoryInterface
{
    /**
     * CurrencyBalanceRepositoryInterface constructor.
     * @param CurrencyBalanceModel $model
     */
    public function __construct(CurrencyBalanceModel $model);

    /**
     * @param int $userAccount
     * @return int
     */
    public function getBalance(int $userAccount): int;

    /**
     * @param int $value
     * @param int $UserAccount
     * @return bool
     */
    public function hasCurrency(int $value, int $UserAccount): bool;

    /**
     * @param int $userAccount
     */
    public function accountExistsOrFail( int $userAccount): void;
}
