<?php
declare(strict_types=1);

namespace Src\Application\CurrencyBalance;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Src\Infrastructure\Controllers\BaseController;

/**
 * Class CurrencyController
 * @package Src\Application\Currency
 */
class CurrencyBalanceController extends BaseController
{
    /** @var string */
    protected $serviceClass = CurrencyBalanceService::class;

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getBalance(ServerRequestInterface $request) : ResponseInterface
    {
        $body = $request->getParsedBody();
        $user['balance'] = $this->newService()->getBalance();
        return $this->buildResponse($user, 200);
    }
}
