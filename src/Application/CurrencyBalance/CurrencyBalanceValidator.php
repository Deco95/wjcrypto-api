<?php
declare(strict_types=1);

namespace Src\Application\CurrencyBalance;

use Src\Infrastructure\Validator\BaseValidator;
use Src\Infrastructure\Validator\ValidatorInterface;
use Src\Units\Exceptions\ValidationException;
use Src\Units\FieldValidator;

/**
 * Class CategoryValidator
 * @package Src\Validator
 */
class CurrencyBalanceValidator extends BaseValidator implements ValidatorInterface
{
    /**
     * CategoryValidator constructor.
     * @param string|null $mode
     * @param int|null    $modelId
     */
    public function __construct(?string $mode, ?int $modelId = null)
    {
        parent::__construct($mode, $modelId);
        $this->model = new CurrencyBalanceModel();
    }

    /**
     * @return array
     */
    public function getCreateValidator(): array
    {
        return [
            'account_number' => new FieldValidator(false, true,  'integer', true),
            'total_balance' => new FieldValidator(false, true,  'integer', false),
            'total_debit' => new FieldValidator(false, true,  'integer', false),
            'total_credit' => new FieldValidator(false, true,  'integer', false),
        ];
    }

    /**
     * @return array
     */
    public function getUpdateValidator(): array
    {
        return [
            'account_number' => new FieldValidator(false, false,  'integer', true),
            'total_balance' => new FieldValidator(false, false,  'integer', false),
            'total_debit' => new FieldValidator(false, false,  'integer', false),
            'total_credit' => new FieldValidator(false, false,  'integer', false),
        ];
    }
}
