<?php
declare(strict_types=1);

namespace Src\Application\CurrencyBalance;

use Src\Infrastructure\Models\BaseModel;

/**
 * Class to use Eloquent ORM.
 *
 * Class CurrencyModel
 * @package Src\Application\Currency
 */
class CurrencyBalanceModel extends BaseModel
{
    /**
     * The database's User's Account field.
     *
     * @var string
     */
    const TABLE_ID = 'id';

    /**
     * The database's User's Account Number field.
     *
     * @var string
     */
    const ACCOUNT_NUMBER = 'account_number';

    /**
     * The database's User's Account BALANCE field.
     *
     * @var string
     */
    const ACCOUNT_BALANCE = 'total_balance';

    /**
     * The database's User's Account field.
     *
     * @var string
     */
    const TOTAL_ACCOUNT_DEBIT = 'total_debit';

    /**
     * The database's User's Account field.
     *
     * @var string
     */
    const TOTAL_ACCOUNT_CREDIT = 'total_credit';

    /**
     * The database's table name.
     *
     * @var string
     */
    protected $table = 'currency_balance';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ACCOUNT_NUMBER,
        self::ACCOUNT_BALANCE,
        self::TOTAL_ACCOUNT_DEBIT,
        self::TOTAL_ACCOUNT_CREDIT,
    ];

    /**
     * @var string
     */
    protected $primaryKey = self::TABLE_ID;
}
