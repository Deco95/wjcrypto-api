<?php
declare(strict_types=1);

namespace Src\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Diactoros\Response\RedirectResponse;
use Src\Application\User\UserService;
use Src\Units\Handlers\SessionHandler;

/**
 * Middleware Interface responsible for implementing user authentication
 *
 * Class AuthMiddleware
 * @package Src\Middleware
 */
class AuthMiddleware implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $auth = false;
        $user = $this->basicAuthentication();
        if (!is_null($user))
            $auth = true;
        // if user has auth, use the request handler to continue to the next
        // middleware and ultimately reach your route callable
        if ($auth === true) {
            session()->start();
            session()->writeFlash('userId',$user['userId']);
            session()->writeFlash('userAccount',$user['account_number']);
//            $session = new SessionHandler(true);
//            $session->writeFlash('userId',$userId);
            return $handler->handle($request);
        }

        // if user does not have auth, possibly return a redirect response,
        // this will not continue to any further middleware and will never
        // reach your route callable
        $uri = $request->getUri();
        return new RedirectResponse(
            $uri->withPath('/auth'),
            401,
            ['WWW-Authenticate'=>'Basic realm="WJCrypto"']
        );
    }

    /**
     * @throws \Exception
     */
    public function basicAuthentication(): ?array
    {
        $email = $_SERVER['PHP_AUTH_USER'];
        $pass = $_SERVER['PHP_AUTH_PW'];
        if ($pass)
            $pass = md5($pass);
        $userService = new UserService();
        return $userService->auth($email, $pass);
    }

    /**
     * @throws \Exception
     */
    public function __destruct()
    {
        session()->handleDestruct();
    }
}
