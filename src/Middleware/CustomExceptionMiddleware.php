<?php
declare(strict_types=1);

namespace Src\Middleware;

use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Diactoros\Response\RedirectResponse;
use Src\Units\Exceptions\BaseException;
use Src\Units\Exceptions\ValidationException;
use Src\Units\Handlers\SessionHandler;

/**
 * Middleware Interface responsible for implementing user authentication
 *
 * Class AuthMiddleware
 * @package Src\Middleware
 */
class CustomExceptionMiddleware implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
//            $session = new SessionHandler(true);
//            $session->writeFlash('userId',$userId);

        try {
            $response = $handler->handle($request);
        } catch (BaseException $error) {
            $response = $this->handleError($request,$error);
        }

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @param BaseException $error
     * @return ResponseInterface
     */
    public function handleError(ServerRequestInterface $request, BaseException $error): ResponseInterface
    {
        /* The router will return the 422 */
        if ($error instanceof ValidationException) {
            return $this->unprocessableEntity($error->getMessage());
        }

        /* The router will throw the 4xx || 500 */
        $httpCode = $error->getCode();
        if (empty($httpCode))
            $httpCode = 500;
        $body = [
                'status_code'  => $httpCode,
                'reason_phrase' => $error->getMessage(),
            ];
        return $this->buildResponse($body,$httpCode);
    }

    /**
     * @param string $message
     * @return ResponseInterface
     */
    public function unprocessableEntity(string $message): ResponseInterface
    {
        $httpCode = 422;
        $error = json_decode($message, true);
        $body = [
            'status_code' => $httpCode,
            'validation' => $error['validation'],
//            'data' => $error['data'],
            'message' => 'Invalid data!',
        ];
        return $this->buildResponse($body,$httpCode);
    }

    /**
     * Build a response using an array, http status code and PSR-7 Response Interface.
     *
     * @param array $body
     * @param int $httpCode
     * @return ResponseInterface
     */
    private function buildResponse(array $body, $httpCode = 500): ResponseInterface
    {
        if (empty($httpCode))
            $httpCode = 500;
        $response = new Response;
        $response->getBody()->write(json_encode($body));
        return $response->withStatus($httpCode)->withAddedHeader('Content-Type','application/json');
    }
}
