<?php
declare(strict_types=1);

namespace Src\Infrastructure\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BaseModel
 * @package Src\Infrastructure\Models
 * @method get($columns = ['*']) Execute the query as a "select" statement.
 * @method where($column, $operator = null, $value = null)
 * @method find($id, $columns = ['*']) Find a model by its primary key.
 * @method findOrFail($id, $columns = ['*']) Find a model by its primary key or throw an exception.
 * @method firstOrFail($id, $columns = ['*']) Find a model by its primary key or throw an exception.
 * @method firstOrCreate(array $array)
 */
abstract class BaseModel extends Model
{
    /**
     * @param array $body
     * @return static
     * @throws \Exception
     */
    public static function fromArray(array $body): self
    {
        $model = get_called_class();
        $model = new $model;
        if (!isset($model->fillable))
            throw new \Exception("Model should declare the parameter 'fillable'");

        $attrs = [];
        foreach ($body as $field => $value) {
            if (isset($model->fillable[$field])) {
                $attrs = array_merge($attrs, [
                    $model->fillable[$field] => $value
                ]);
            }
        }

        return $model->fill($attrs);
    }

    /**
     * @param array $values
     * @return bool
     */
    public function exists(array $values): bool
    {
        return boolval($this->where($values)->first());
    }

    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = '';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    public $fillableUpdate = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
