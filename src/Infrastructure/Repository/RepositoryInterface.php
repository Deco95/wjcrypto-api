<?php
declare(strict_types=1);

namespace Src\Infrastructure\Repository;

use stdClass;

/**
 * Interface RepositoryInterface
 * @package Src\Infrastructure\Repository
 */
interface RepositoryInterface
{
    /**
     * Cria uma nova instância da Model cujo nome é passado em $this->modelClass e
     * popula com os dados do $array passado.
     *
     * @param array $data
     */
    function factory(array $data = []);

    /**
     * Retorna uma Colletions com os dados dos registros do banco de acordo com os parâmetros passados
     *
     * @param int $take
     * @param bool $paginate
     * @param array $params
     * @return mixed
     */
    public function getAll(int $take = 15, bool $paginate = true, array $params = []);

    /**
     * Retorna um registro de acordo com o $id passado no parâmetro,
     * caso esse $id não exista retorna uma not found exception.
     *
     * @param int $id
     */
    public function getById(int $id);

    /**
     * @param array $data
     */
    public function create(array $data = []);

    /**
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data = []);

    /**
     * Deleta o registro cujo o id seja o mesmo do Model passado por parâmetro.
     *
     * @param int $id
     */
    public function delete(int $id);

    /**
     * Restaura o registro com o $id passado por parâmetro,
     * caso esse $id não exista retorna um not found exception.
     *
     * @param int $id
     */
    public function restore(int $id) ;
}
