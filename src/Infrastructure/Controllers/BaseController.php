<?php
declare(strict_types=1);

namespace Src\Infrastructure\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Laminas\Diactoros\Response;
use Src\Infrastructure\Services\BaseService;

/** Default Controller for return json data with Flash data from session.
 *
 * Class BaseController
 * @package Src\Infrastructure\Controllers
 */
class BaseController
{
    /** @var string */
    protected $serviceClass = BaseService::class;

    /** @var Response */
    private $response;

    public function __construct(ResponseInterface $response = null)
    {
        $this->response = $response;
        if ($response === null)
            $this->response = new Response;
    }

    /**
     * @return BaseService
     */
    protected function newService(): BaseService
    {
        return new $this->serviceClass;
    }

    /**
     * @param $data
     * @param int $code
     * @return ResponseInterface
     * @throws \Exception
     */
    public function buildResponse($data , int $code = 200) : ResponseInterface
    {
        if (is_array($data)) {
            $data = json_decode(json_encode($data));
        }
        $returnArray = [
            'status_code' => $code,
            'data' => $data,
        ];
//        $returnArray = $this->getFlashData($returnArray);

        $this->response->getBody()->write(json_encode($returnArray));
        return $this->response->withStatus($code);
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    protected function getFlashData(array $data): array
    {
        $data = $this->getValidFlashData($data, 'message');

        $data = $this->getValidFlashData($data, 'validation');

//        $data = $this->getValidFlashData($data, 'data');

        return $data;
    }

    /**
     * @param array $data
     * @param string $name
     * @return array
     * @throws \Exception
     */
    protected function getValidFlashData(array $data, string $name): array
    {
        $flashData = session()->readFlash($name);

        if ($flashData !== null && ! empty($flashData)) {
            $data[$name] = $flashData;
        }

        return $data;
    }
}
