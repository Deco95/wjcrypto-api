<?php

namespace Src\Infrastructure\Validator;

use Src\Infrastructure\Models\BaseModel;
use Src\Units\FieldValidator;

/**
 * Class BaseValidator
 * @package app\Infrastructure\Validator
 */
abstract class BaseValidator
{
    /** @var BaseModel */
    protected $model;

    /** @var string */
    const CREATE_MODE = 'create';

    /** @var string */
    const UPDATE_MODE = 'update';

    /** @var string */
    const FIELD_REQUIRED_ERROR = 'This field is required!';

    /** @var string */
    const FIELD_TYPE_ERROR = 'This field must be %s!';

    /** @var string */
    const FIELD_UNIQUE_ERROR = 'This field must be unique!';

    /** @var string */
    const FIELD_EXISTS_ERROR = 'This field must exist!';

    /** @var string */
    const TYPE_DELIMITER = ',';

    /** @var string */
    protected $mode;

    /** @var int */
    protected $modelId;

    /** @var array */
    protected $fieldsValidation = [];

    /**
     * BaseValidator constructor.
     * @param null|string $mode
     * @param null|int    $modelId
     */
    public function __construct(?string $mode = self::CREATE_MODE, ?int $modelId = null)
    {
        $this->mode = $mode;
        $this->modelId = $modelId;

        if ($this->mode === self::CREATE_MODE) {
            $this->fieldsValidation = $this->getCreateValidator();
        }

        if ($this->mode === self::UPDATE_MODE) {
            $this->fieldsValidation = $this->getUpdateValidator();
        }
    }


    /**
     * @param FieldValidator $fieldValidator
     * @param string $field
     * @param array $data
     * @return string
     */
    protected function customValidatorBefore(FieldValidator $fieldValidator, string $field, array $data): string
    {
        return '';
    }

    /**
     * @param FieldValidator $fieldValidator
     * @param string $field
     * @param array $data
     * @return string
     */
    protected function customValidatorAfter(FieldValidator $fieldValidator, string $field, array $data): string
    {
        return '';
    }

    /**
     * @param array $data
     * @return array
     */
    public function validate(array $data): array
    {
        $errors = [];

        foreach ($this->fieldsValidation as $field => $fieldValidator) {
            $beforeErrors = $this->customValidatorBefore($fieldValidator, $field, $data);
            if (!empty($beforeErrors)) {
                $errors[$field] = $beforeErrors;
                break;
            }

            if ($fieldValidator->isRequired()) {
                if (!$this->hasField($fieldValidator, $field, $data)) {
                    $errors[$field] = self::FIELD_REQUIRED_ERROR;
                break;
                }

                $fieldTypeError = $this->validateFieldType($fieldValidator, $field, $data);
                if ($fieldTypeError !== null) {
                    $errors[$field] = $fieldTypeError;
                break;
                }

                if (!$this->isUnique($fieldValidator, $field, $data)) {
                    $errors[$field] = self::FIELD_UNIQUE_ERROR;
                break;
                }

                if (!$this->isExists($fieldValidator, $field, $data)) {
                    $errors[$field] = self::FIELD_EXISTS_ERROR;
                break;
                }
            }

            $afterErrors = $this->customValidatorAfter($fieldValidator, $field, $data);
            if (!empty($afterErrors)) {
                $errors[$field] = $afterErrors;
                break;
            }
        }

        return $errors;
    }

    /**
     * @param FieldValidator $fieldValidator
     * @param string $field
     * @param array $data
     * @return bool
     */
    protected function hasField(FieldValidator $fieldValidator, string $field, array $data): bool
    {
        if (!$fieldValidator->isRequired()) {
            return true;
        }

        return isset($data[$field]);
    }

    /**
     * @param FieldValidator $fieldValidator
     * @param string $field
     * @param array $data
     * @return string|null
     */
    public function validateFieldType(FieldValidator $fieldValidator, string $field, array $data): ?string
    {
        $types = explode(self::TYPE_DELIMITER, $fieldValidator->getTypes());

        $isType = false;

        foreach ($types as $type) {
            if (gettype($data[$field]) === $type) {
                $isType = true;
                break;
            }
        }

        if (!$isType) {
            $typesStringError = implode(' or ', $types);
            return sprintf(self::FIELD_TYPE_ERROR, $typesStringError);
        }

        return null;
    }

    /**
     * @param FieldValidator $fieldValidator
     * @param string $field
     * @param array $data
     * @return bool
     */
    public function isUnique(FieldValidator $fieldValidator, string $field, array $data): bool
    {
        if (!$fieldValidator->isUnique()) {
            return true;
        }

        if ($this->mode === self::UPDATE_MODE) {
            $register = $this->model->find($this->modelId);
            if ($register->$field === $data[$field]) {
                return true;
            }
        }

        return !$this->model->exists([$field => $data[$field]]);
    }

    /**
     * @param FieldValidator $fieldValidator
     * @param string $field
     * @param array $data
     * @return bool
     */
    public function isExists(FieldValidator $fieldValidator, string $field, array $data): bool
    {
        if (!$fieldValidator->isExists()) {
            return true;
        }

        return $this->model->exists([$field => $data[$field]]);
    }

    /**
     * @return array
     */
    abstract public function getCreateValidator(): array;

    /**
     * @return array
     */
    abstract public function getUpdateValidator(): array;
}
