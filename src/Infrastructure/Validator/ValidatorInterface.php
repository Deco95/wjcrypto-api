<?php
declare(strict_types=1);

namespace Src\Infrastructure\Validator;

/**
 * Interface CurrencyRepositoryInterface
 * @package Src\Application\Currency
 */
interface ValidatorInterface
{
    /**
     * BaseValidatorInterface constructor.
     * @param string|null $mode
     * @param int|null $modelId
     */
    public function __construct(?string $mode, ?int $modelId = null);

    /**
     * @param array $data
     * @return array
     */
    public function validate(array $data): array;

    /**
     * @return array
     */
    public function getCreateValidator(): array;

    /**
     * @return array
     */
    public function getUpdateValidator(): array;
}
