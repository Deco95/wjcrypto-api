<?php
declare(strict_types=1);

namespace Src\Infrastructure\Services;

use Src\Infrastructure\Models\BaseModel;
use Src\Infrastructure\Repository\BaseRepository;
use Src\Infrastructure\Repository\RepositoryInterface;
use Src\Infrastructure\Validator\BaseValidator;
use Src\Infrastructure\Validator\ValidatorInterface;
use Src\Units\Exceptions\BaseException;
use Src\Units\Exceptions\ValidationException;
use Exception;

/**
 * Class BaseService
 * @package Src\Infrastructure\Services
 */
abstract class BaseService
{
    /** @var string */
    protected $modelClass = BaseModel::class;

    /** @var string */
    protected $validatorClass = ValidatorInterface::class;

    /** @var string */
    protected $repositoryClass = RepositoryInterface::class;

    /**
     * @return BaseModel
     */
    protected function newModel(): BaseModel
    {
        return new $this->modelClass;
    }

    /**
     * @param string|null $mode
     * @param int|null    $modelId
     * @return BaseValidator
     */
    protected function newValidator(?string $mode, ?int $modelId = null): BaseValidator
    {
        return new $this->validatorClass($mode, $modelId);
    }

    /**
     * @return BaseRepository
     */
    protected function newRepository(): BaseRepository
    {
        return new $this->repositoryClass($this->newModel());
    }

    /**
     * @param string $mode
     * @param array $valuesFiltered
     * @param int|null $id
     * @throws ValidationException
     */
    public function validate(string $mode, array $valuesFiltered, ?int $id = null): void
    {
        $validator = $this->newValidator($mode, $id);
        $validationErrors = $validator->validate($valuesFiltered);
        if (!empty($validationErrors)) {
            $errors = [
                'data' => $valuesFiltered,
                'validation' => $validationErrors,
            ];
            throw new ValidationException(json_encode($errors), 422);
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function getUserFromSession(): ?array
    {
        $userId = session()->readFlash('userId');
        $userAccount = session()->readFlash('userAccount');

        if (!$userId or !$userAccount)
            return null;
        return [
            'userId' => $userId,
            'account' => $userAccount,
        ];
    }

    /**
     * @return int|null
     * @throws Exception
     */
    public static function getUserAccount(): ?int
    {
        $userAccount = session()->readFlash('userAccount');
        if (!$userAccount)
            return null;
        return $userAccount;
    }

    /**
     * @return int|null
     * @throws Exception
     */
    public static function getUserId(): ?int
    {
        $userId = session()->readFlash('userId');
        if (!$userId)
            return null;
        return $userId;
    }
}
