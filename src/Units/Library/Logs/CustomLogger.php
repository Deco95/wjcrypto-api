<?php
declare(strict_types=1);

namespace Src\Units\Library\Logs;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

/**
 * Class CustomLogger
 * @package Src\Units\Library\Logs
 */
class CustomLogger extends Logger
{
    /**
     * Create a custom Monolog instance.
     *
     * @param $name
     * @param array $handlers
     * @param array $processors
     */
    public function __construct($name, array $handlers = array(), array $processors = array())
    {
        // Create the logger
        parent::__construct($name, $handlers, $processors);
        // Now add some handlers
        $this->pushHandler(new StreamHandler(ROOT_PATH.'/app/storage/logs/wjcrypto.log', Logger::DEBUG));
        $this->pushHandler(new FirePHPHandler());
    }
}
