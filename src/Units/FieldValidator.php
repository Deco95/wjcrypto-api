<?php

namespace Src\Units;

/**
 * Class FieldValidator
 * @package Src\Units
 */
class FieldValidator
{
    /** @var bool */
    protected $before;

    /** @var bool */
    protected $required;

    /** @var string */
    protected $types;

    /** @var bool */
    protected $unique;

    /** @var bool */
    protected $exists;

    /** @var bool */
    protected $after;

    /**
     * FieldValidator constructor.
     * @param bool   $before
     * @param bool   $required
     * @param string $types
     * @param bool   $unique
     * @param bool   $exists
     * @param bool   $after
     */
    public function __construct(
        bool $before = false,
        bool $required = false,
        string $types = '',
        bool $unique = false,
        bool $exists = false,
        bool $after = false
    )
    {
        $this->before = $before;
        $this->required = $required;
        $this->types = $types;
        $this->unique = $unique;
        $this->exists = $exists;
        $this->after = $after;
    }

    /**
     * @return bool
     */
    public function isBefore(): bool
    {
        return $this->before;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @return string
     */
    public function getTypes(): string
    {
        return $this->types;
    }

    /**
     * @return bool
     */
    public function isUnique(): bool
    {
        return $this->unique;
    }

    /**
     * @return bool
     */
    public function isExists(): bool
    {
        return $this->exists;
    }

    /**
     * @return bool
     */
    public function isAfter(): bool
    {
        return $this->after;
    }
}
