<?php declare(strict_types=1);

define('ROOT_PATH', realpath(__DIR__.'/../../'));

require_once ROOT_PATH.'/vendor/autoload.php';
require_once ROOT_PATH.'/app/routes/web.php';
require_once __DIR__.'/database/bootstrap.php';

$request = Laminas\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$response = $router->dispatch($request);

// send the response to the browser
(new Laminas\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);
