<?php

namespace App\bootstrap\database;

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Class DatabaseEloquent
 */
class DatabaseEloquent
{
    /**
     * @return void
     */
    public static function setup(): void
    {
        $capsule = new Capsule;

        $capsule->addConnection([
            "driver"   => DB_CONNECTION,
            "host"     => DB_HOST,
            "port"     => DB_PORT,
            "database" => DB_DATABASE,
            "username" => DB_USERNAME,
            "password" => DB_PASSWORD,
        ]);

        // Make this Capsule instance available globally.
        $capsule->setAsGlobal();

        // Setup the Eloquent ORM.
        $capsule->bootEloquent();
        $capsule->bootEloquent();
    }
}
