<?php
declare(strict_types=1);

namespace App\bootstrap\database;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class DatabaseDoctrine
{
    /**
     * @var array
     */
    private $connectionOptions;
    /**
     * @var bool
     */
    private $isDevMode;

    public function __construct()
    {
        $this->isDevMode = false;

        $this->connectionOptions = array(
            "driver"   => DB_CONNECTION,
            "host"     => DB_HOST,
            "port"     => DB_PORT,
            "database" => DB_DATABASE,
            "username" => DB_USERNAME,
            "password" => DB_PASSWORD,
        );

    }

    public function  newEntityManager(): EntityManager
    {
        $config = Setup::createConfiguration($this->isDevMode);
        return EntityManager::create($this->connectionOptions, $config);
    }
}
