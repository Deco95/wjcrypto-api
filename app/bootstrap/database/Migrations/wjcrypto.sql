create table transaction
(
    id int auto_increment,
    transaction_type varchar(255) null,
--     transaction_status varchar(255) null,
    account_debit int null,
    account_credit int null,
    debit int unsigned null,
    credit int unsigned null,
    created_at datetime null,
    updated_at datetime null,
    deleted_at datetime null,
    constraint transaction_pk
        primary key (id)
);

create table currency_balance
(
    id int auto_increment,
    account_number int unsigned not null,
    total_balance int null,
    total_debit int unsigned null,
    total_credit int unsigned null,
    created_at datetime null,
    updated_at datetime null,
    deleted_at datetime null,
    constraint currency_pk
        primary key (id)
);

create table user
(
    id int auto_increment,
--     account_number int unsigned not null unique,
    name varchar(255) null,
    email varchar(255) not null,
    taxvat varchar(255) not null unique,
    password varchar(255) not null,
    registry varchar(255) not null unique,
    birthday varchar(255) null,
    phone varchar(255) null,
    address varchar(255) null,
    created_at datetime null,
    updated_at datetime null,
    deleted_at datetime null,
    constraint user_pk
        primary key (id)
);


