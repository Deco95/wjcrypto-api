<?php

use App\bootstrap\database\DatabaseEloquent;

$envPath = ROOT_PATH.'/.env';
$env = parse_ini_file($envPath);

define('DB_CONNECTION', $env['DB_CONNECTION']);
define('DB_HOST' , $env['DB_HOST']);
define('DB_PORT' , $env['DB_PORT']);
define('DB_DATABASE' , $env['DB_DATABASE']);
define('DB_USERNAME' , $env['DB_USERNAME']);
define('DB_PASSWORD' , $env['DB_PASSWORD']);
define('DB_CHARSET' , $env['DB_CHARSET']);
define('APP_TIMEZONE' , $env['APP_TIMEZONE']);

DatabaseEloquent::setup();
