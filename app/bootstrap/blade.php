<?php

use eftec\bladeone\BladeOne;


//$blade = new BladeOne($views,$cache,BladeOne::MODE_AUTO);
function blade() { // What is it? A singleton.
    $views = ROOT_PATH . '/storage/views';
    $cache = ROOT_PATH . '/storage/cache';
    global $blade;
    if ($blade===null) {
        $blade = new BladeOne($views,$cache,BladeOne::MODE_AUTO);
    }
    return $blade;
}

// So you can call it everywhere as
//echo blade()->run("folder.template",[]);
