<?php

use Laminas\Diactoros\Response;

if (!function_exists('response')) {
    /**
     * @return Response
     */
    function response(): Response
    {
        return new Response;
    }
}


if (! function_exists('isValidJson')) {
    /**
     * @param $string
     * @return bool
     */
    function isValidJson($string) {
        if (empty($string)) return false;
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}


if (! function_exists('session')) {
    /**
     * @return \Src\Units\Handlers\SessionHandler
     */
    function session(): \Src\Units\Handlers\SessionHandler
    {
        return new \Src\Units\Handlers\SessionHandler();
    }
}


if (! function_exists('endsWith')) {
    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function endsWith(string $haystack, string $needle): bool
    {
        return substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }
}

if (! function_exists('dd')) {
    /**
     * @param $dump
     * @return void
     */
    function dd($dump): void
    {
        var_dump($dump);
        die();
    }
}
