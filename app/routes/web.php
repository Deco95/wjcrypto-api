<?php
/**
 * This file contains all the routes for the project
 */

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Src\Application\CurrencyBalance\CurrencyBalanceController;
use Src\Application\Transaction\TransactionController;
use Src\Application\User\UserController;

$responseFactory = new \Laminas\Diactoros\ResponseFactory();
$strategy = new League\Route\Strategy\JsonStrategy($responseFactory);
$router   = (new League\Route\Router)->setStrategy($strategy);

// API Home
$router->map('GET', '/', function (ServerRequestInterface $request) : ResponseInterface {
    $response = new Laminas\Diactoros\Response;
    $body = [
        'title'   => 'WJCrypto API',
        'message' => 'WebJump Academy',
        'version' => '0.0.1',
    ];
    $response->getBody()->write(json_encode($body));
    return $response->withStatus(200);
});

// API User Routes
$router->map('POST', '/register',[UserController::class, 'register'])
    ->middleware(new Src\Middleware\CustomExceptionMiddleware);
$router->group('/user', function (\League\Route\RouteGroup $route) {
    $route->map('GET', '/',[UserController::class, 'getUser']);
    $route->map('POST', '/update',[UserController::class, 'update']);
})->middleware(new Src\Middleware\AuthMiddleware)->middleware(new Src\Middleware\CustomExceptionMiddleware);

// API Transaction Routes
$router->group('/transaction', function (\League\Route\RouteGroup $route) {
    $route->map('GET', '/transactions',[TransactionController::class, 'getTransactions']);
    $route->map('POST', '/debit',[TransactionController::class, 'debit']);
    $route->map('POST', '/credit',[TransactionController::class, 'credit']);
    $route->map('POST', '/transfer',[TransactionController::class, 'transfer']);
})->middleware(new Src\Middleware\AuthMiddleware)->middleware(new Src\Middleware\CustomExceptionMiddleware);

// API Currency Routes
$router->group('/currency', function (\League\Route\RouteGroup $route) {
    $route->map('GET', '/balance',[CurrencyBalanceController::class, 'getBalance']);
})->middleware(new Src\Middleware\AuthMiddleware)->middleware(new Src\Middleware\CustomExceptionMiddleware);
